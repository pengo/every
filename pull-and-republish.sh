#!/bin/bash
set -e
# Any subsequent(*) commands which fail will cause the shell script to exit immediately

# recompile and regenerate files
cd ~/every
git pull
./build-debug.sh
mono ~/every/bin/Debug/every.exe > ~/public_html/index.html
