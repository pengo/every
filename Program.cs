﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
The MIT License (MIT)
Copyright (c) 2016 Peter Halasz

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/
namespace every {
    class Program {
        static void Main(string[] args) {
            //Console.Write("Content-Type: text/plain\n\n");


            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            Console.OutputEncoding = System.Text.Encoding.Unicode;
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            Console.Out.NewLine = "\n";

            //Console.Write("Content-Type: text/html\n\n");

            //new Categories().ShowCats();

            Console.WriteLine("<!DOCTYPE HTML>");
            Console.WriteLine("<html><head>");
            Console.WriteLine("<title>Hello</title>");
            Console.WriteLine("<meta charset=\"UTF-8\" />");
            Console.WriteLine("</head><body>");
            Console.WriteLine("<h1>Hello, world 8.</h1>");

            new Categories().ShowCats();

            new WikidataQuery().TestQuery();

            Console.WriteLine("<!-- Elapsed time: " + stopWatch.Elapsed.TotalSeconds + " seconds -->");

            Console.WriteLine("</body></html>");
        }
    }
}
