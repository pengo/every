﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using System.IO;
using System.IO.Compression;
using System.Net;

/*
The MIT License (MIT)
Copyright (c) 2016 Peter Halasz

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

namespace every {
    public class WikidataQuery {

        public void TestQuery() {
            //con "Q11845374"
            Console.WriteLine("Wikimedia disambiguation page Q11845374: " + WhatIsItem(11845374) + " <br/>");
            Console.WriteLine("Northern Screamer Q997306: " + WhatIsItem(997306) + " <br/>");
            Console.WriteLine("Western Atlantic seabream Q2700401: " + WhatIsItem(2700401) + " <br/>");
            Console.WriteLine("Melanie L. DeVore Q21341935: " + WhatIsItem(21341935) + " <br/>");
            Console.WriteLine("Nadia de la Rosa Q21387667: " + WhatIsItem(21387667) + " <br/>");
            Console.WriteLine("universe Q1: " + WhatIsItem(1) + " <br/>");
            Console.WriteLine("Finn Borchsenius (Q1417600) " + WhatIsItem(11417600) + " <br/>");
            //
        }

        public uint WhatIsItem(uint item) {
            uint property = 31;
            string urlTemplate = @"http://wdq.wmflabs.org/api?q=items[{0}]&props={1}"; // https works too

            //RestSharp.Contrib.HttpUtility.UrlEncode(index), 
            //json is different to: https://www.wikidata.org/wiki/Special:EntityData/Q42.json

            string uriString = string.Format(urlTemplate, item, property);

            try {
                WebRequest request = WebRequest.Create(uriString);
                WebResponse response = request.GetResponse();

                StreamReader input = null;
                using (Stream responseStream = response.GetResponseStream()) {
                    if (uriString.EndsWith(".gz")) {
                        GZipStream gzstream = new GZipStream(responseStream, CompressionMode.Decompress, true);
                        input = new StreamReader(gzstream, Encoding.UTF8); //or Encoding.UNICODE  ?

                    } else {
                        input = new StreamReader(responseStream, Encoding.UTF8); // Encoding.Unicode?
                    }

                    string page = input.ReadToEnd();

                    //WikidataJson json = JsonConvert.DeserializeObject<WikidataJson>(page);
                    dynamic json = JsonConvert.DeserializeObject(page);
                    //dynamic json = Json.Decode(page); // System.Web.Helpers

                    if (json == null) {
                        // fail, retry?
                        throw new Exception("no result or not found.");
                    }

                    // if it fails, it will just throw an exception and that's fine.

                    //TODO: batch requests
                    uint value = json["props"][property.ToString()][0][2];

                    return value;

                }
            } catch (Exception e) {
                throw e;
                //Console.Error.WriteLine(e.Message);
                //return false;
            }

        }
    }
    /*
    [DataContract]
    public class WikidataJson {

        // one item:
        //{"status":{"error":"OK","items":1,"querytime":"0ms","parsed_query":"ITEMS[2]"},
        // "items":[2],
        // "props":{"31":[[2,"item",3504248]]}
        //}

        // two items:
        //{"status":{"error":"OK","items":2,"querytime":"0ms","parsed_query":"ITEMS[24]"},
        // "items":[2,4],
        // "props":{"31":[[4,"item",937228],[2,"item",3504248]]}
        //}

        // this json format is awful. should be something like: (flat format)
        // "items": {"Q2": {"label-en": "the universe", "P31": "Q3504248"}, "Q4": {"P31": "Q937228"} }
        // or.. (more full)
        // "items": {"Q2": {"label": {"en": "The universe", "ja": "yuuniversa"}, "properties": { "property": "P31", value: "Q3504248", value-type: "item"} }

        //or like the official format:
        // https://www.mediawiki.org/wiki/Wikibase/DataModel/JSON

        //[DataMember]
        //public Dictionary<uint,Prop[]> props;
    }

    public class Prop {
        public uint item;
        public string type; // "item" or "property"
        public uint value;
    }
    */
}