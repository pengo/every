﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/*
The MIT License (MIT)
Copyright (c) 2016 Peter Halasz

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

namespace every.src.labsdb {
    class EveryDB {

        //TODO: automatically create table if needed. (For now just run in mysql manually):
        // become every
        // mysql --defaults-file="${HOME}"/replica.my.cnf -h tools-db s52832__every_p
        string create_items_table_sql =
@"CREATE TABLE `s52832__every_p`.`items` (
  `item_id` INT UNSIGNED NOT NULL,
  `item_row_last_updated` DATETIME NULL,
  `item_quick_label` VARCHAR(45) NULL,
  `item_isa` INT NOT NULL,
  `item_subclass_of` INT NOT NULL,
  `item_gender` INT NOT NULL,
  `item_taxon` VARCHAR(45) NULL,
  `item_taxon_level` VARCHAR(45) NULL,
  `item_wikis_list` VARCHAR(45) NULL,
  `item_wikis_count` INT NULL,
  `item_pedias_count` INT NULL,
  `item_is_on_enwiki` TINYINT(1) NULL,
  PRIMARY KEY (`item_id`),
  UNIQUE INDEX `item_id_UNIQUE` (`item_id` ASC));
";

        public void AddItem(uint item) {

        }

        public void RefreshItem(uint item) {

        }

    }
}
