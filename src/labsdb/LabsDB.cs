﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IniParser;
using IniParser.Model;

/*
The MIT License (MIT)
Copyright (c) 2016 Peter Halasz

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

namespace every {
    class LabsDB {

        static LabsDB labsDB;

        private string user;
        private string pass;
        string homePath;

        public static LabsDB Instance() {
            if (labsDB == null) {
                labsDB = new LabsDB();
                labsDB.Init();
            }

            return labsDB;
        }

        private void Init() {
            homePath = Environment.GetEnvironmentVariable("HOME");

            //if (string.IsNullOrWhiteSpace(homePath)) { // only in .NET Framework 4.6 and 4.5.. simplify for compatibility
            if (string.IsNullOrEmpty(homePath)) {
                homePath = "/data/project/every-other-wiki-has";
            }

            string configFile = homePath + "/replica.my.cnf";

            // ini-parser (nuget) [MIT License]
            var iniparser = new FileIniDataParser();
            IniData inidata = iniparser.ReadFile(configFile);
            user = inidata["client"]["user"];
            pass = inidata["client"]["password"];
        }

        public MySql.Data.MySqlClient.MySqlConnection WikidataConnection() {
            return DBConnection("wikidatawiki");
        }

        public MySql.Data.MySqlClient.MySqlConnection EveryConnection() {
            MySql.Data.MySqlClient.MySqlConnection conn;
            string myConnectionString;

            string server = "tools-db";
            string dbname = "s52832__every_p";

            myConnectionString = "server=" + server
                + ";uid=" + user
                + ";pwd=" + pass
                + ";database=" + dbname
                + ";charset=utf8"
                + ";";

            try {
                conn = new MySql.Data.MySqlClient.MySqlConnection();
                conn.ConnectionString = myConnectionString;
                conn.Open();

                return conn;

            } catch (MySql.Data.MySqlClient.MySqlException ex) {

                Console.WriteLine(ex.Message);
                return null; // TODO: throw error
            }
        }

        public MySql.Data.MySqlClient.MySqlConnection DBConnection(string site) {
            MySql.Data.MySqlClient.MySqlConnection conn;
            string myConnectionString;

            string server = site + ".labsdb"; // e.g. wikidatawiki.labsdb, enwiki.labsdb
            string dbname = site + "_p"; // e.g. wikidatawiki_p, enwiki_p

            myConnectionString = "server=" + server
                + ";uid=" + user
                + ";pwd=" + pass
                + ";database=" + dbname 
                + ";charset=utf8"
                + ";";

            try {
                conn = new MySql.Data.MySqlClient.MySqlConnection();
                conn.ConnectionString = myConnectionString;
                conn.Open();

                return conn;

            } catch (MySql.Data.MySqlClient.MySqlException ex) {

                Console.WriteLine(ex.Message);
                return null; // TODO: throw error
            }

        }
    }
}
