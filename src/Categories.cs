﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/*
The MIT License (MIT)
Copyright (c) 2016 Peter Halasz

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

namespace every {
    class Categories {

        //TODO: cats and pages per site
        // private HashSet<string> cats; // temporary

        string findRedLinksForSite;

        private HashSet<UInt32> pages;
        private HashSet<UInt32> excludedSitePages; //TODO

        /*
        // not used

        string[] majorLanguages = { "en", "de", "ja", "ru", "es", "fr", "it", "pt", "zh", "pl" };
        // TODO: automatically query meta_p for "english" sites
        string[] preferredForEnglishSpeakers = { "enwiki", "en", "simple", "species", "commons", // english
        "de", "fr", "es", "pt", "fr", "it", "pl"};  // major languages with latin script

        // languages which don't use Latin scripts (not exhaustive)
        string[] leastPreferredForEnglishSpeakers = { "zh", "ja", "he", "ar", "ckb", "ur", "fa", "ru", "uk", "am", "arz", "as", "ba", "be_x_old", "be", "bg", "bn", "bpy", "cv", "el", "gan", "gu", "hi", "hy", "ka", "kk", "km", "ko", "ky", "lrc", "mai", "ml", "mr", "mrj", "mn", "my", "ne", "new", "or", "os", "pa", "pn", "pnb", "ps", "rue", "sah", "sa", "si", "ta", "te", "th", "tt", "ug", "ur", "wu", "wuu", "xmf", "yi", "zh_yue",
        "bat_smg", "lv", "bs"};
        */

        MySqlConnection wikidataConn; // cache connection queries that aren't recursive and don't call other queries

        public void ShowCats(string category = "Category:Botanists", string excludesite = "enwiki") {
            this.findRedLinksForSite = excludesite;

            try {
                UInt32 item_id = ItemIdAnyLang(category);
                if (item_id == 0) {
                    Console.WriteLine("Not found: " + category);
                    return;
                }

                foreach (var sitepage in Sitepages(item_id)) {
                    //TODO

                    string site = sitepage.site;
                    string page = sitepage.page;

                    Console.WriteLine("<h2>" + site + ": " + sitepage.page + "</h2>");

                    //TODO: deal with weird stuff.

                    if (site.EndsWith("books")) continue;
                    if (site.EndsWith("news")) continue;
                    if (site.EndsWith("quote")) continue;
                    if (site.EndsWith("source")) continue;
                    if (site.EndsWith("versity")) continue;
                    if (site.EndsWith("voyage")) continue;
                    if (site == "species") continue;
                    if (site == "commons") continue;

                    //TODO: auto transcribe
                    // e.g. fawiki: رابرت_براون_(گیاه‌شناس) — گیاه‌شناسان_اهل_اسکاتلند
                    // https://fa.wikipedia.org/wiki/%D8%B1%D8%A7%D8%A8%D8%B1%D8%AA_%D8%A8%D8%B1%D8%A7%D9%88%D9%86_(%DA%AF%DB%8C%D8%A7%D9%87%E2%80%8C%D8%B4%D9%86%D8%A7%D8%B3)
                    // e.g. ukwiki: Карл_Клузіус — Ботаніки
                    // https://uk.wikipedia.org/wiki/%D0%9A%D0%B0%D1%80%D0%BB_%D0%9A%D0%BB%D1%83%D0%B7%D1%96%D1%83%D1%81
                    // e.g. elwiki: Ζαν_ντε_Τεβενό — Γάλλοι_βοτανολόγοι
                    // https://el.wikipedia.org/wiki/%CE%96%CE%B1%CE%BD_%CE%BD%CF%84%CE%B5_%CE%A4%CE%B5%CE%B2%CE%B5%CE%BD%CF%8C

                    // hide non-people entries: 
                    // e.g. jawiki: 植物学者の略記一覧_(D-G) — 植物学者
                    // instance of (P31) Wikimedia list article (Q13406463)
                    // https://www.wikidata.org/wiki/Q17993581
                    // eg. nowiki: Dagpåfugløye — Dyr formelt beskrevet av Carl von Linné [=Category:Animal described by Carl Linnaeus]
                    // https://no.wikipedia.org/wiki/Dagp%C3%A5fugl%C3%B8ye
                    // eg. eswiki: Mustela lutreola — Especies descritas por Linnaeus
                    // instance of (P31) taxon (Q16521)
                    // e.g. instance of (P31) Wikimedia disambiguation page (Q4167410)
                    // ruwikisource:
                    // Ботанический словарь (Анненков) / Amomum grana Paradisii / ДО — Ботанический словарь(Анненков) < br />
                    // Keep: instance of (P31) Human (Q5) -- but what about siblings, etc
                    // e.g. Elekiter in ja Category:平賀源内 (Hiraga Gennai)

                    //exclusion groups?
                    // main ignore category: Category:Horticulturists and gardeners
                    // Thomas Jefferson — American gardeners
                    // Edward La Trobe Bateman — English landscape and garden designers
                    // Amy Aiken — American horticulturists
                    // Nicolás García Uriburu — Landscape architects
                    // Johnny Miller — Golf course architects

                    //DONE: hide redirects
                    // rowiki: Bercht. (biolog) — Abrevieri referitoare la autori biologi fără articol
                    // https://ro.wikipedia.org/w/index.php?title=Bercht._(biolog)&redirect=no

                    // transcribe / (translate?)
                    // ジョージ・キング_(植物学者) — スコットランドの植物学者

                    LoadPages(site, page); // all sites
                }

                //LoadPages(); // just enwiki

                //Console.WriteLine("Total cats searched: " + cats.Count() + "<br/>");
                Console.WriteLine("Total pages found: " + pages.Count() + "<br/>");

            } catch (Exception e) {
                Console.WriteLine("Exception: {0}", e.ToString());
            } finally {

                if (wikidataConn != null) {
                    wikidataConn.Close();
                    wikidataConn = null;
                }
            }
        }

        //TODO: category is hard coded
        public UInt32 ItemIdAnyLang(string category) {
            var conn = LabsDB.Instance().WikidataConnection();

            try {
                using (conn) {
                    // ips_row_id | ips_item_id | ips_site_id | ips_site_page
                    //TODO: check if more than one result row, and if any have a different ips_item_id
                    //TODO: prepared statement
                    string sql = "select ips_item_id, ips_site_id from wb_items_per_site where ips_site_page = \"Category:Botanists\" limit 1;";

                    MySqlCommand cmd = new MySqlCommand(sql, conn);
                    MySqlDataReader rdr = cmd.ExecuteReader();

                    UInt32 item_id;
                    string site_id;

                    if (rdr.Read()) {
                        item_id = rdr.GetUInt32(0); //  rdr.GetString(0);
                        site_id = rdr.GetString(1); // e.g. enwiki

                        Console.WriteLine(item_id + ": "
                            + site_id + "<br/>");

                        return item_id;

                    } else {
                        return 0;
                    }
                }

            } catch (MySqlException ex) {
                Console.WriteLine("Error: {0}", ex.ToString());
            }

            return 0;
        }

        MySqlCommand sitepagesCmd;
        //returns: site_id ("enwiki"), site_page ("Category:Botany")
        public IEnumerable<PageInfo> Sitepages(UInt32 itemID) {
            if (wikidataConn == null) {
                wikidataConn = LabsDB.Instance().WikidataConnection();
            }
            var conn = wikidataConn;

            // ips_row_id | ips_item_id | ips_site_id | ips_site_page

            if (sitepagesCmd == null) {
                string sql = "select ips_site_id, convert(ips_site_page using utf8) as ips_site_page from wb_items_per_site where ips_item_id = @item ;";
                sitepagesCmd = new MySqlCommand(sql, conn);
                sitepagesCmd.Parameters.Add("@item", MySqlDbType.UInt32);
                sitepagesCmd.Prepare();
            } else {

                sitepagesCmd.Connection = conn;
                //sitepagesCmd.Prepare();
            }

            sitepagesCmd.Parameters["@item"].Value = itemID;

            //string sql = "select ips_site_id, ips_site_page from wb_items_per_site where ips_item_id = " + itemID + ";";

            MySqlDataReader rdr = sitepagesCmd.ExecuteReader();

            List<PageInfo> pages = new List<PageInfo>();

            using (rdr) {


                while (rdr.Read()) {

                    string site_id = rdr.GetString(0); // e.g. enwiki
                    string site_page = rdr.GetString(1);

                    pages.Add(new PageInfo(site_id, site_page));

                    //var b = (byte[])rdr[1];
                    //site_page = Encoding.UTF8.GetString(rdr.GetBytes(1));

                    //Console.WriteLine(site_id + ": " + site_page + "<br/>");

                    //yield return Tuple.Create(site_id, site_page); // don't yield so can reuse connection?
                }

                // TODO: if none:
                //    Console.WriteLine("Not found: " + itemID);
            }

            return pages.AsEnumerable();
        }

        public string SitepagesList(UInt32 itemID) {
            var sitepages = Sitepages(itemID).ToArray();
            StringBuilder str = new StringBuilder();

            //if (excludesite != null && sitepages.Any(p => (p.Item1 == excludesite))) {


            //} else {

            foreach (var sitepage in sitepages) {
                string site = sitepage.site;
                string page = sitepage.page;

                //TODO: make url
                //Console.Write("<a href=''>{0}</a>");

                string shortsite = site;
                if (site.EndsWith("wiki")) {
                    shortsite = site.Substring(0, site.Length - 4);
                }

                str.Append(shortsite + " ");

            }
            //}

            return str.ToString();
        }

        public UInt32 cat_page_id(string site = "enwiki", string page = "Category:Botanists") {

            string pageShort = page;
            if (page.Contains(':')) {
                int cut = page.IndexOf(':');
                pageShort = page.Substring(cut + 1);
            }

            string sql = "SELECT page_id "
                + " FROM page "
                + " WHERE page_title = " + pageShort // TODO FIXME
                + " AND page_namespace = 14; ";


            var conn = LabsDB.Instance().DBConnection(site);
            using (conn) {

                MySqlCommand cmd = new MySqlCommand(sql, conn);
                MySqlDataReader rdr = cmd.ExecuteReader();

                if (rdr.Read()) {
                    return rdr.GetUInt32(0);
                }
            }

            return 0;
        }

        public void LoadPages(string site = "enwiki", string cat_page_name = "Botanists", int depth = 9, HashSet<string> doneCats = null) {


            if (pages == null) {
                pages = new HashSet<UInt32>();
            }

            if (doneCats == null) {
                doneCats = new HashSet<string>();
            }

            string cat_pageShort = cat_page_name.Replace(' ', '_'); ;
            if (cat_pageShort.Contains(':')) {
                int cut = cat_pageShort.IndexOf(':');
                cat_pageShort = cat_pageShort.Substring(cut + 1);
            }


            // categorylinks.cl_to 
            // Stores the name (excluding namespace prefix) of the desired category. Spaces are replaced by underscores (_)
            // e.g. "Redirects_with_old_history"

            // categorylinks.cl_from e.g. 13

            //TODO: cache compiled version of statement?
            string sql = "SELECT DISTINCT page_id, convert(page_title using utf8), cl_type "
                + " FROM page "
                + " JOIN categorylinks ON page_id = cl_from "
                + " WHERE cl_to = @catshort "
                + " AND cl_type in ('page', 'subcat') "
                + " AND page_namespace in (0, 14) "  // only interested in actual pages or subcats
                + " AND page_is_redirect = 0; "; // excludes for example, [[w:ro:Bercht. (biolog)]] //TODO: make optional

            //cl_type
            //file, subcat (subcategory) or page (normal page)). 

            var conn = LabsDB.Instance().DBConnection(site);
            
            using (conn) {

                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@catshort", cat_pageShort);
                cmd.Prepare();
                MySqlDataReader rdr = cmd.ExecuteReader();

                UInt32 page_id;
                string page_title;
                string type;

                while (rdr.Read()) {
                    page_id = rdr.GetUInt32(0);
                    page_title = rdr.GetString(1);
                    type = rdr.GetString(2);

                    if (type == "subcat") {
                        //TODO: save up subqueries and do together
                        if (!doneCats.Contains(page_title)) {
                            doneCats.Add(page_title);
                            LoadPages(site, page_title, depth - 1, doneCats);
                        }


                    } else if (type == "page") {
                        UInt32 wd_id = WikidataID(site, page_title);
                        string label = BestItemLabel(wd_id, page_title.Replace('_', ' '));
                        if (wd_id != 0 && !pages.Contains(wd_id)) {
                            pages.Add(wd_id);

                            // TODO: not like this. Make a proper list or something.
                            Console.WriteLine(
                                "<a href='https://www.wikidata.org/wiki/Q{0}'>{1}</a>"
                                + " — " + cat_pageShort.Replace('_', ' ')
                                + " — " + SitepagesList(wd_id)
                                + "<br/>",
                                wd_id,
                                label
                            );
                        }

                    }
                }

                // TODO: if none:
                //    Console.WriteLine("Not found: " + itemID);
            }
        }


        MySqlCommand wikidataIdCmd = null;
        public UInt32 WikidataID(string site, string pagename) {

            if (wikidataConn == null) {
                wikidataConn = LabsDB.Instance().WikidataConnection();
            }
            var conn = wikidataConn;

            //using (conn) { // keep this one open

            string page = pagename.Replace('_', ' ');

            if (wikidataIdCmd == null) {
                string sql =
    @"select ips_item_id 
    from wikidatawiki_p.wb_items_per_site 
    where ips_site_page = @page 
    and ips_site_id = @site;";

                wikidataIdCmd = new MySqlCommand(sql, conn);
                wikidataIdCmd.Parameters.AddWithValue("@site", site);
                wikidataIdCmd.Parameters.AddWithValue("@page", page);
                //wikidataIdCmd.Parameters.Add("@site");
                //wikidataIdCmd.Parameters.Add("@page");
                wikidataIdCmd.Prepare();
            } else {
                wikidataIdCmd.Parameters.Clear();
                //wikidataIdCmd.Parameters["@site"].Value = site; // error?
                //wikidataIdCmd.Parameters["@page"].Value = page;
                wikidataIdCmd.Parameters.AddWithValue("@site", site);
                wikidataIdCmd.Parameters.AddWithValue("@page", page);

            }


            MySqlDataReader rdr = wikidataIdCmd.ExecuteReader();

            using (rdr) {
                if (rdr.Read()) {

                    UInt32 wikidata_id = rdr.GetUInt32(0);
                    return wikidata_id;
                }

                return 0;
            }
        }


        MySqlCommand ItemLabelCmd = null;
        public string BestItemLabel(uint item, string defaultLabel) {
            // find the best label you can, preferably english or at least in Latin script

            //wb_terms

            if (wikidataConn == null) {
                wikidataConn = LabsDB.Instance().WikidataConnection();
            }
            var conn = wikidataConn;

            // ips_row_id | ips_item_id | ips_site_id | ips_site_page

            if (ItemLabelCmd == null) {
                //string sql = "select ips_site_id, convert(ips_site_page using utf8) as ips_site_page from wb_items_per_site where ips_item_id = @item ;";
                string sql =
                         "select convert(term_language using utf8) as term_language, convert(term_text using utf8) as term_text "
                       + " from wikidatawiki_p.wb_terms "
                       + " where term_entity_id = @item " // e.g. 41688 for Hendrik Lorentz (Q41688)
                       + " AND term_entity_type = 'item' "
                       // + " AND term_language = 'en' "  // use ORDER BY instead
                       + " AND term_type = 'label' "
                       //TODO: add all Latin-script languages? or do an unlimited query if preferred language not found and search for a latin-looking one?
                       + " ORDER BY FIELD(term_language, 'sl', 'sk', 'br', 'ay', 'ang', 'als', 'af', 'pl', 'it', 'pt', 'fr', 'la', 'es', 'de', 'en') desc " // put most favoured at end
                       + " LIMIT 1 ;";

                ItemLabelCmd = new MySqlCommand(sql, conn);
                ItemLabelCmd.Parameters.Add("@item", MySqlDbType.UInt32);
                ItemLabelCmd.Prepare();

            } else {

                ItemLabelCmd.Connection = conn;
                //sitepagesCmd.Prepare();
            }

            ItemLabelCmd.Parameters["@item"].Value = item;

            //string sql = "select ips_site_id, ips_site_page from wb_items_per_site where ips_item_id = " + itemID + ";";

            MySqlDataReader rdr = ItemLabelCmd.ExecuteReader();

            List<PageInfo> pages = new List<PageInfo>();

            using (rdr) {


                if (rdr.Read()) {

                    string lang = rdr.GetString(0); // e.g. en
                    string label = rdr.GetString(1); // e.g. Doris Duke

                    if (!string.IsNullOrEmpty(label))
                        return label;
                }

            }

            return defaultLabel;
        }


    }
}